package io.uvera.pmsuproject.seller.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.seller.recyclerview.SellerMyArticlesAdapter
import io.uvera.pmsuproject.util.ioLaunch
import io.uvera.pmsuproject.util.motionToastApiError
import io.uvera.pmsuproject.util.motionToastApiException
import io.uvera.pmsuproject.util.ui
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class SellerArticlesActivity : SellerBaseActivity() {
    private val api: SellerMyArticlesApi by inject()

    private lateinit var adapter: SellerMyArticlesAdapter

    private lateinit var noArticlesTextView: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_articles)

        noArticlesTextView = findViewById(R.id.sellerMyArticlesNoneTextView)

        recyclerView = findViewById(R.id.recyclerViewSellerMyArticles)
        adapter = SellerMyArticlesAdapter(api, lifecycleScope) { fetchFromApiAndSet() }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter

        noArticlesTextView.visibility = View.GONE
        recyclerView.visibility = View.GONE

        fetchFromApiAndSet()
    }

    override fun onResume() {
        super.onResume()
        fetchFromApiAndSet(true)
    }

    private fun fetchFromApiAndSet(silent: Boolean = false) = lifecycleScope.ioLaunch {
        api.getMyArticles()
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            recyclerView.visibility = View.GONE
                            noArticlesTextView.visibility = View.VISIBLE
                        } else {
                            recyclerView.visibility = View.VISIBLE
                            noArticlesTextView.visibility = View.GONE
                            adapter.setItems(d)
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    recyclerView.visibility = View.GONE
                    noArticlesTextView.visibility = View.VISIBLE
                    if (!silent)
                        this@SellerArticlesActivity.motionToastApiError()
                }
            }.suspendOnException {
                ui {
                    recyclerView.visibility = View.GONE
                    noArticlesTextView.visibility = View.VISIBLE
                    if (!silent)
                        this@SellerArticlesActivity.motionToastApiException()
                }
            }
    }

}