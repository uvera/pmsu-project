package io.uvera.pmsuproject.seller.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerSalesApi
import io.uvera.pmsuproject.seller.recyclerview.SellerSalesAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class SellerSalesActivity : SellerBaseActivity() {
    private val api: SellerSalesApi by inject()

    private lateinit var adapter: SellerSalesAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var noSalesTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_sales)

        adapter = SellerSalesAdapter()
        noSalesTextView = findViewById(R.id.sellerSalesNoneTextView)
        recyclerView = findViewById(R.id.recyclerViewSellerSales)
        with(recyclerView) {
            layoutManager = LinearLayoutManager(this@SellerSalesActivity)
            addItemDecoration(
                DividerItemDecoration(
                    this@SellerSalesActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@SellerSalesActivity.adapter
        }
        noSalesTextView.visibility = View.GONE
        recyclerView.visibility = View.GONE
        fetchFromApiAndSet()
    }

    override fun onResume() {
        super.onResume()
        fetchFromApiAndSet()
    }

    private fun fetchFromApiAndSet() = lifecycleScope.ioLaunch {
        api.getAllSales().suspendOnSuccess {
            ui {
                data?.let { d ->
                    if (d.isEmpty()) {
                        recyclerView.visibility = View.GONE
                        noSalesTextView.visibility = View.VISIBLE
                    } else {
                        recyclerView.visibility = View.VISIBLE
                        noSalesTextView.visibility = View.GONE
                        adapter.setItems(d)
                    }
                }
            }
        }.suspendOnError {
            ui {
                recyclerView.visibility = View.GONE
                noSalesTextView.visibility = View.VISIBLE
                this@SellerSalesActivity.motionToastApiError()
            }
        }.suspendOnException {
            ui {
                recyclerView.visibility = View.GONE
                noSalesTextView.visibility = View.VISIBLE
                this@SellerSalesActivity.motionToastApiException()
            }
        }
    }
}