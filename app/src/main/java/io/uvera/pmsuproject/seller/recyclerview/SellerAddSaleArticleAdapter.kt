package io.uvera.pmsuproject.seller.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerMyArticleAddSale

class SellerAddSaleArticleAdapter : RecyclerView.Adapter<SellerAddSaleArticleViewHolder>() {
    private var dataSet: List<SellerMyArticleAddSale> = listOf()

    fun setItems(items: List<SellerMyArticleAddSale>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SellerAddSaleArticleViewHolder =
        SellerAddSaleArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_seller_add_sale_articles, parent, false
            )
        )

    override fun onBindViewHolder(holder: SellerAddSaleArticleViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class SellerAddSaleArticleViewHolder(
    v: View,
) : RecyclerView.ViewHolder(v) {
    private val checkBox: CheckBox = v.findViewById(R.id.sellerAddSaleArticleCheckBox)
    private var current: SellerMyArticleAddSale? = null

    fun bind(it: SellerMyArticleAddSale) {
        current = it
        checkBox.isChecked = it.checked
        checkBox.text = it.name
    }

    init {
        checkBox.setOnClickListener {
            it as CheckBox
            current?.checked = it.isChecked
        }
    }
}