package io.uvera.pmsuproject.seller.activity

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.globalDateFormatter
import io.uvera.pmsuproject.seller.api.SellerMyArticleAddSale
import io.uvera.pmsuproject.seller.recyclerview.SellerAddSaleArticleAdapter
import io.uvera.pmsuproject.seller.viewmodel.SellerAddSaleViewModel
import io.uvera.pmsuproject.util.*
import www.sanju.motiontoast.MotionToast
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

class SellerAddSaleActivity : ShakeableSellerBaseActivity() {
    companion object {
    }

    private var listOfArticles: List<SellerMyArticleAddSale> = listOf()
    private lateinit var recyclerView: RecyclerView
    private lateinit var startDate: EditText
    private lateinit var button: Button
    private val adapter = SellerAddSaleArticleAdapter()
    private val viewModel: SellerAddSaleViewModel by viewModels()

    private val startDateLocalDate: LocalDate?
        get() = if (this::startDate.isInitialized) try {
            LocalDate.parse(startDate.text, globalDateFormatter)
        } catch (e: DateTimeParseException) {
            null
        } else null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_add_sale)
        supportActionBar?.hide()
        button = findViewById(R.id.sellerAddSaleAddButton)
        startDate = findViewById(R.id.sellerAddSaleStartDate)
        bindEditTextWithDatePicker(R.id.sellerAddSaleStartDate)
        bindEditTextWithDatePicker(R.id.sellerAddSaleEndDate)

        recyclerView = findViewById(R.id.sellerAddSaleRecyclerView)
        with(recyclerView) {
            layoutManager = LinearLayoutManager(this@SellerAddSaleActivity)
            addItemDecoration(
                DividerItemDecoration(
                    this@SellerAddSaleActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@SellerAddSaleActivity.adapter
        }
        form {
            input(R.id.sellerAddSalePercent, "percent") {
                isNumber()
                    .atLeast(1)
                    .atMost(99)
            }
            input(R.id.sellerAddSaleStartDate, "startDate") {
                isNotEmpty()
                assert("Date should be at least today") {
                    val text = it.text
                    try {
                        val date = LocalDate.parse(text, globalDateFormatter)
                        if (date.isAfter(LocalDate.now().minusDays(1)))
                            return@assert true
                    } catch (e: DateTimeParseException) {
                        return@assert false
                    }
                    false
                }
            }
            input(R.id.sellerAddSaleEndDate, "endDate") {
                isNotEmpty()
                assert("End date cannot be before start date") {
                    val text = it.text
                    try {
                        val date = LocalDate.parse(text, globalDateFormatter)

                        val startDate = startDateLocalDate ?: return@assert false
                        if (startDate.isBefore(date)) return@assert true
                    } catch (e: DateTimeParseException) {
                        return@assert false
                    }
                    false
                }
            }
            input(R.id.sellerAddSaleDesc, "text") {
                isNotEmpty()
            }
            submitWith(R.id.sellerAddSaleAddButton) {
                submitToApi(it, listOfArticles)
            }
        }
        fetchArticlesFromApiAndSet()
    }

    override fun hearShake() {
        button.performClick()
    }

    private fun submitToApi(it: FormResult, articles: List<SellerMyArticleAddSale>) {
        if (articles.none { it.checked }) {
            this@SellerAddSaleActivity.motionToastApiError("Please select at least a single article")
            return
        }
        lifecycleScope.ioLaunch {
            viewModel.sendToApi(it, articles.map { it.id })
                .suspendOnSuccess {
                    finish()
                }.suspendOnError {
                    this@SellerAddSaleActivity.motionToastApiError()
                }.suspendOnException {
                    this@SellerAddSaleActivity.motionToastApiException()
                }
        }
    }

    private fun fetchArticlesFromApiAndSet() = lifecycleScope.ioLaunch {
        viewModel.getArticles()
            .suspendOnSuccess {
                data?.let { d ->
                    if (d.isEmpty()) {
                        this@SellerAddSaleActivity.motionToastApiError("No articles fetched")
                        return@suspendOnSuccess
                    }
                    val articles = d.map { i -> SellerMyArticleAddSale(i) }
                    ui {
                        adapter.setItems(articles)
                    }
                    listOfArticles = articles
                }
            }.suspendOnError {
                this@SellerAddSaleActivity.motionToastApiError("Unknown error, failed to fetch articles")
            }.suspendOnException {
                this@SellerAddSaleActivity.motionToastApiException("Network error, failed to fetch articles")
            }

    }
}