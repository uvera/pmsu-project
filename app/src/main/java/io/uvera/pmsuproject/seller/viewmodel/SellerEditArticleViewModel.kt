package io.uvera.pmsuproject.seller.viewmodel

import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import io.uvera.pmsuproject.seller.api.SellerMyArticleEdit
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.util.trimmedString
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SellerEditArticleViewModel : ViewModel(), KoinComponent {
    private val api: SellerMyArticlesApi by inject()

    suspend fun fetchArticleById(id: Long) = api.getMyArticleForEdit(id)

    suspend fun updateArticle(id: Long, f: FormResult) =
        api.updateMyArticle(
            id,
            SellerMyArticleEdit(
                f["name"]?.trimmedString() ?: "",
                f["description"]?.trimmedString() ?: "",
                f["price"]?.asDouble() ?: 0.0
            )
        )
}