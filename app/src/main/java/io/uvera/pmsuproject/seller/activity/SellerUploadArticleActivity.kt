package io.uvera.pmsuproject.seller.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.lifecycleScope
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import com.bumptech.glide.Glide
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.viewmodel.SellerUploadArticleViewModel
import io.uvera.pmsuproject.util.*
import www.sanju.motiontoast.MotionToast

class SellerUploadArticleActivity : ShakeableSellerBaseActivity() {
    private val viewModel: SellerUploadArticleViewModel by viewModels()
    private val imageFromGallery =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let { it ->
                Glide.with(this)
                    .load(it)
                    .into(imageView)
            }
        }

    private lateinit var imageView: ImageView
    private lateinit var uploadButton: Button
    private val drawable: Drawable?
        get() = if (this::imageView.isInitialized) imageView.drawable else null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_upload_article)

        supportActionBar?.hide()

        imageView = findViewById(R.id.sellerUploadMyArticleImageView)
        uploadButton = findViewById(R.id.sellerUploadMyArticleUploadButton)
        findViewById<Button>(R.id.sellerUploadMyArticleGalleryButton).setOnClickListener {
            clickGalleryButton()
        }
        form {
            useRealTimeValidation()
            input(R.id.sellerUploadMyArticleName, "name") {
                isNotEmpty()
            }
            input(R.id.sellerUploadMyArticleDescription, "description") {
                isNotEmpty()
            }
            input(R.id.sellerUploadMyArticlePrice, "price") {
                isDecimal()
                    .atLeast(1.0)
                    .atMost(1000000.0)
            }
            submitWith(R.id.sellerUploadMyArticleUploadButton) {
                sendForm(it)
            }
        }
    }

    override fun hearShake() {
        uploadButton.performClick()
    }

    private fun sendForm(f: FormResult) = lifecycleScope.ioLaunch {
        if (drawable == null) {
            ui {
                this@SellerUploadArticleActivity.motionToastApiError("You didn't provide the image")
            }
            return@ioLaunch
        }
        ui {
            uploadButton.setGrayed(true)
        }
        viewModel.uploadImage(f, (drawable as BitmapDrawable).bitmap)
            .suspendOnSuccess {
                finish()
            }.suspendOnError {
                ui {
                    this@SellerUploadArticleActivity.motionToastApiError()
                    uploadButton.setGrayed(false)
                }
            }.suspendOnException {
                ui {
                    this@SellerUploadArticleActivity.motionToastApiException()
                    uploadButton.setGrayed(false)
                }
            }
    }

    private fun clickGalleryButton() = imageFromGallery.launch("image/*")

}