package io.uvera.pmsuproject.seller.viewmodel

import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.globalDateFormatter
import io.uvera.pmsuproject.seller.api.SellerAddSale
import io.uvera.pmsuproject.seller.api.SellerMyArticleAddSale
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.seller.api.SellerSalesApi
import io.uvera.pmsuproject.util.parseLocalDateOrNull
import io.uvera.pmsuproject.util.trimmedString
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.time.LocalDate

class SellerAddSaleViewModel : ViewModel(), KoinComponent {
    private val articleApi: SellerMyArticlesApi by inject()
    private val saleApi: SellerSalesApi by inject()

    suspend fun getArticles() = articleApi.getMyArticles()
    suspend fun sendToApi(f: FormResult, listOfArticles: List<Long>) = saleApi.addSale(
        SellerAddSale(
            f["percent"]?.asInt() ?: 0,
            f["startDate"]?.trimmedString() ?: "",
            f["endDate"]?.trimmedString() ?: "",
            f["text"]?.trimmedString() ?: "",
            listOfArticles
        )
    )
}