package io.uvera.pmsuproject.seller.api

import com.skydoves.sandwich.ApiResponse
import okhttp3.MultipartBody
import okhttp3.Request
import okhttp3.RequestBody
import retrofit2.http.*
import java.time.LocalDate

private const val BASE_PATH = "/seller-orders"

interface SellerCommentsApi {
    @GET(BASE_PATH)
    suspend fun getComments(): ApiResponse<List<SellerComment>>

    @DELETE("$BASE_PATH/{id}")
    suspend fun archiveComment(@Path("id") id: Long): ApiResponse<Void>

}

class SellerComment(
    val id: Long,
    val time: String,
    val comment: String,
    val rating: Int,
    val sender: String,
)