package io.uvera.pmsuproject.seller.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerComment
import io.uvera.pmsuproject.seller.api.SellerCommentsApi
import io.uvera.pmsuproject.util.ioLaunch
import io.uvera.pmsuproject.util.setGrayed
import io.uvera.pmsuproject.util.ui
import kotlinx.coroutines.CoroutineScope
import www.sanju.motiontoast.MotionToast

class SellerCommentsAdapter(
    private val api: SellerCommentsApi,
    private val lifecycleScope: CoroutineScope,
    private val reloadCallback: () -> Unit,
    private val motionToastError: () -> Unit,
    private val motionToastException: () -> Unit,
) : RecyclerView.Adapter<SellerCommentsViewHolder>() {
    private var dataSet: List<SellerComment> = listOf()

    fun setItems(items: List<SellerComment>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SellerCommentsViewHolder =
        SellerCommentsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_seller_comments, parent, false
            ), api, reloadCallback, lifecycleScope, motionToastError, motionToastException
        )

    override fun onBindViewHolder(holder: SellerCommentsViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class SellerCommentsViewHolder(
    v: View,
    private val api: SellerCommentsApi,
    private val reloadCallback: () -> Unit,
    private val lifecycleScope: CoroutineScope,
    private val motionToastError: () -> Unit,
    private val motionToastException: () -> Unit,
) : RecyclerView.ViewHolder(v) {
    private val senderTextView: TextView = v.findViewById(R.id.sellerCommentsSender)
    private val dateTextView: TextView = v.findViewById(R.id.sellerCommentsDate)
    private val ratingTextView: TextView = v.findViewById(R.id.sellerCommentsRating)
    private val commentTextView: TextView = v.findViewById(R.id.sellerCommentsComment)
    private val archiveButton: Button = v.findViewById(R.id.sellerCommentsArchiveButton)

    private var current: SellerComment? = null

    fun bind(it: SellerComment) {
        current = it
        senderTextView.text = it.sender
        dateTextView.text = it.time
        ratingTextView.text = it.rating.toString()
        commentTextView.text = it.comment
    }

    init {
        archiveButton.setOnClickListener {
            current?.let { c ->
                clickArchiveButton(c)
            }
        }
    }

    private fun clickArchiveButton(c: SellerComment) = lifecycleScope.ioLaunch {
        ui {
            archiveButton.setGrayed(true)
        }
        api.archiveComment(c.id)
            .suspendOnSuccess {
                ui {
                    archiveButton.setGrayed(false)
                }
                reloadCallback()
            }.suspendOnError {
                ui {
                    archiveButton.setGrayed(false)
                    motionToastError()
                }
            }.suspendOnException {
                ui {
                    archiveButton.setGrayed(false)
                    motionToastException()
                }
            }
    }
}