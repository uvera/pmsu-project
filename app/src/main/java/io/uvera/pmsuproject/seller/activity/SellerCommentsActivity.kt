package io.uvera.pmsuproject.seller.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerCommentsApi
import io.uvera.pmsuproject.seller.recyclerview.SellerCommentsAdapter
import io.uvera.pmsuproject.seller.recyclerview.SellerMyArticlesAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class SellerCommentsActivity : SellerBaseActivity() {
    private val api: SellerCommentsApi by inject()
    private lateinit var adapter: SellerCommentsAdapter

    private lateinit var noCommentsTextView: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_comments)

        adapter = SellerCommentsAdapter(
            api,
            lifecycleScope,
            { fetchFromApiAndSet() },
            { motionToastError() },
            { motionToastException() }
        )
        noCommentsTextView = findViewById(R.id.sellerCommentsNoneTextView)
        recyclerView = findViewById(R.id.recyclerViewSellerComments)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter

        noCommentsTextView.visibility = View.GONE
        recyclerView.visibility = View.GONE
        fetchFromApiAndSet()
    }

    override fun onResume() {
        super.onResume()
        fetchFromApiAndSet()
    }

    private fun motionToastException() {
        this.motionToastApiException()
    }

    private fun motionToastError() {
        this.motionToastApiError()
    }

    private fun fetchFromApiAndSet() = lifecycleScope.ioLaunch {
        api.getComments()
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            recyclerView.visibility = View.GONE
                            noCommentsTextView.visibility = View.VISIBLE
                        } else {
                            recyclerView.visibility = View.VISIBLE
                            noCommentsTextView.visibility = View.GONE
                            adapter.setItems(d)
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    recyclerView.visibility = View.GONE
                    noCommentsTextView.visibility = View.VISIBLE
                    this@SellerCommentsActivity.motionToastError()
                }
            }.suspendOnException {
                ui {
                    recyclerView.visibility = View.GONE
                    noCommentsTextView.visibility = View.VISIBLE
                    this@SellerCommentsActivity.motionToastException()
                }

            }
    }

}