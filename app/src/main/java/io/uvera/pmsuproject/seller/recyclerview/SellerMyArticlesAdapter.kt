package io.uvera.pmsuproject.seller.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.uvera.pmsuproject.BASE_URL
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.activity.SellerEditArticleActivity
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.seller.api.SellerMyArticle
import io.uvera.pmsuproject.util.intentOf
import io.uvera.pmsuproject.util.ioLaunch
import io.uvera.pmsuproject.util.setGrayed
import io.uvera.pmsuproject.util.ui
import kotlinx.coroutines.CoroutineScope

class SellerMyArticlesAdapter(
    private val api: SellerMyArticlesApi,
    private val lifecycleScope: CoroutineScope,
    private val reloadCallback: () -> Unit,
) : RecyclerView.Adapter<SellerMyArticlesViewHolder>() {
    private var dataSet: List<SellerMyArticle> = listOf()

    fun setItems(items: List<SellerMyArticle>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SellerMyArticlesViewHolder =
        SellerMyArticlesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_seller_my_article, parent, false
            ), api, reloadCallback, lifecycleScope
        )

    override fun onBindViewHolder(holder: SellerMyArticlesViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class SellerMyArticlesViewHolder(
    v: View,
    private val api: SellerMyArticlesApi,
    private val reloadCallback: () -> Unit,
    private val lifecycleScope: CoroutineScope,
) : RecyclerView.ViewHolder(v) {
    private val nameTextView: TextView = v.findViewById(R.id.sellerMyArticleName)
    private val descriptionTextView: TextView = v.findViewById(R.id.sellerMyArticleDescription)
    private val priceTextView: TextView = v.findViewById(R.id.sellerMyArticlePrice)
    private val articleImageView: ImageView = v.findViewById(R.id.sellerMyArticleImage)
    private val deleteButton: Button = v.findViewById(R.id.sellerMyArticleDeleteButton)
    private val editButton: Button = v.findViewById(R.id.sellerMyArticleEditButton)

    private var current: SellerMyArticle? = null

    fun bind(it: SellerMyArticle) {
        current = it
        nameTextView.text = it.name
        descriptionTextView.text = it.description
        priceTextView.text = it.price.toString()
        Glide.with(itemView.context)
            .load("$BASE_URL/images/${it.pathToImage}")
            .centerCrop()
            .placeholder(R.drawable.loading_spinner)
            .into(articleImageView)
    }

    init {
        deleteButton.setOnClickListener {
            current?.let { c ->
                clickDeleteButton(c)
            }
        }
        editButton.setOnClickListener {
            current?.let { c ->
                clickEditButton(c)
            }
        }
    }

    private fun clickEditButton(c: SellerMyArticle) {
        val ctx = itemView.context
        val intent = ctx.intentOf(SellerEditArticleActivity::class).also { i ->
            i.putExtra("id", c.id)
        }
        ctx.startActivity(intent)
    }

    private fun clickDeleteButton(c: SellerMyArticle) = lifecycleScope.ioLaunch {
        ui {
            deleteButton.setGrayed(true)
        }
        api.deleteMyArticle(c.id)
        ui {
            deleteButton.setGrayed(false)
        }
        reloadCallback()
    }
}