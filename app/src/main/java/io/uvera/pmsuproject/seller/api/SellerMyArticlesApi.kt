package io.uvera.pmsuproject.seller.api

import com.skydoves.sandwich.ApiResponse
import okhttp3.MultipartBody
import okhttp3.Request
import okhttp3.RequestBody
import retrofit2.http.*

private const val BASE_PATH = "/seller/articles"

interface SellerMyArticlesApi {
    @GET(BASE_PATH)
    suspend fun getMyArticles(): ApiResponse<List<SellerMyArticle>>

    @DELETE("$BASE_PATH/{id}")
    suspend fun deleteMyArticle(@Path("id") id: Long): ApiResponse<Void>

    @GET("$BASE_PATH/{id}")
    suspend fun getMyArticleForEdit(@Path("id") id: Long): ApiResponse<SellerMyArticleEdit>

    @PUT("$BASE_PATH/{id}")
    suspend fun updateMyArticle(
        @Path("id") id: Long,
        @Body body: SellerMyArticleEdit
    ): ApiResponse<Void>

    @Multipart
    @POST(BASE_PATH)
    suspend fun createMyArticle(
        @Part("name") name: RequestBody,
        @Part("description") description: RequestBody,
        @Part("price") price: RequestBody,
        @Part file: MultipartBody.Part
    ): ApiResponse<Void>
}

open class SellerMyArticle(
    val id: Long,
    val name: String,
    val description: String,
    val price: Double,
    val pathToImage: String,
)

class SellerMyArticleAddSale(val id: Long, val name: String, var checked: Boolean = false) {
    constructor(article: SellerMyArticle) : this(article.id, "${article.name} - ${article.price}$")
}

data class SellerMyArticleEdit(
    val name: String,
    val description: String,
    val price: Double
)