package io.uvera.pmsuproject.seller.activity

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.seller.viewmodel.SellerEditArticleViewModel
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast
import kotlin.properties.Delegates

class SellerEditArticleActivity : SellerBaseActivity() {
    private lateinit var updateButton: Button
    private lateinit var nameEdit: EditText
    private lateinit var descriptionEdit: EditText
    private lateinit var priceEdit: EditText
    private var id by Delegates.notNull<Long>()

    private val viewModel: SellerEditArticleViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_edit_article)

        supportActionBar?.hide()

        id = intent.getLongExtra("id", 0)
        updateButton = findViewById(R.id.sellerEditMyArticleUpdateButton)

        nameEdit = findViewById(R.id.sellerEditMyArticleName)
        descriptionEdit = findViewById(R.id.sellerEditMyArticleDescription)
        priceEdit = findViewById(R.id.sellerEditMyArticlePrice)

        populateFormFromApi(id)
        form {
            input(R.id.sellerEditMyArticleName, "name") {
                isNotEmpty()
            }
            input(R.id.sellerEditMyArticleDescription, "description") {
                isNotEmpty()
            }
            input(R.id.sellerEditMyArticlePrice, "price") {
                isDecimal()
                    .atLeast(1.0)
                    .atMost(1000000.0)
            }
            submitWith(R.id.sellerEditMyArticleUpdateButton) {
                sendFormToApi(id, it)
            }
        }
    }

    private fun populateFormFromApi(id: Long) = lifecycleScope.ioLaunch {
        viewModel.fetchArticleById(id).suspendOnSuccess {
            ui {
                data?.let { d ->
                    nameEdit.setText(d.name)
                    descriptionEdit.setText(d.description)
                    priceEdit.setText(d.price.toString())
                }
            }
        }.suspendOnError {
            ui {
                updateButton.setGrayed(false)
                this@SellerEditArticleActivity.motionToastApiError()
                finish()
            }
        }.suspendOnException {
            ui {
                updateButton.setGrayed(false)
                this@SellerEditArticleActivity.motionToastApiException()
                finish()
            }
        }
    }

    private fun sendFormToApi(id: Long, it: FormResult) = lifecycleScope.ioLaunch {
        ui {
            updateButton.setGrayed(true)
        }
        viewModel.updateArticle(id, it).suspendOnSuccess {
            ui {
                updateButton.setGrayed(false)
                finish()
            }
        }.suspendOnError {
            ui {
                updateButton.setGrayed(false)
                this@SellerEditArticleActivity.motionToastApiError()
            }
        }.suspendOnException {
            ui {
                updateButton.setGrayed(false)
                this@SellerEditArticleActivity.motionToastApiException()
            }
        }
    }


}