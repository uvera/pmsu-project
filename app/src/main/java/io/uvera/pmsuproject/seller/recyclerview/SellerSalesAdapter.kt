package io.uvera.pmsuproject.seller.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.seller.api.SellerSale

class SellerSalesAdapter : RecyclerView.Adapter<SellerSalesViewHolder>() {
    private var dataSet: List<SellerSale> = listOf()

    fun setItems(items: List<SellerSale>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SellerSalesViewHolder =
        SellerSalesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_seller_sales, parent, false
            )
        )

    override fun onBindViewHolder(holder: SellerSalesViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class SellerSalesViewHolder(
    v: View,
) : RecyclerView.ViewHolder(v) {
    private val articlesRecyclerView: RecyclerView =
        v.findViewById(R.id.recyclerViewSellerSaleArticles)
    private val percentView: TextView = v.findViewById(R.id.sellerSalesPercent)
    private val startDateView: TextView = v.findViewById(R.id.sellerSalesStartDate)
    private val endDateView: TextView = v.findViewById(R.id.sellerSalesEndDate)
    private val descView: TextView = v.findViewById(R.id.sellerSalesSaleDesc)
    private var articlesAdapter: SellerSalesArticlesAdapter = SellerSalesArticlesAdapter()

    fun bind(it: SellerSale) {
        percentView.text = "${it.percent}%"
        startDateView.text = it.startDate
        endDateView.text = it.endDate
        descView.text = it.text
        articlesAdapter.setItems(it.articleList)
    }

    init {
        with(articlesRecyclerView) {
            layoutManager = LinearLayoutManager(itemView.context)
            addItemDecoration(
                DividerItemDecoration(
                    itemView.context,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = articlesAdapter
        }
    }

}

class SellerSalesArticlesAdapter : RecyclerView.Adapter<SellerSalesArticlesViewHolder>() {
    private var dataSet: List<String> = listOf()

    fun setItems(items: List<String>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SellerSalesArticlesViewHolder = SellerSalesArticlesViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.recyclerview_seller_sales_articles, parent, false)
    )

    override fun onBindViewHolder(holder: SellerSalesArticlesViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size

}

class SellerSalesArticlesViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val textView: TextView = v.findViewById(R.id.sellerSalesArticleText)

    fun bind(it: String) = with(textView) {
        text = it
    }
}