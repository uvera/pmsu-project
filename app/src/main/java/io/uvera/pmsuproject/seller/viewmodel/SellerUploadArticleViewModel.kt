package io.uvera.pmsuproject.seller.viewmodel

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.ApiResponse
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.util.trimmedString
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.*

class SellerUploadArticleViewModel(application: Application) : AndroidViewModel(application),
    KoinComponent {
    private val api: SellerMyArticlesApi by inject()
    private val TAG = this.javaClass.simpleName

    suspend fun uploadImage(f: FormResult, b: Bitmap): ApiResponse<Void> {
        val bitmapFile = b.asFile(getApplication<Application>().cacheDir)

        val fName = f["name"]?.trimmedString() ?: ""
        val name = fName.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val fDescription = f["description"]?.trimmedString() ?: ""
        val description = fDescription.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val fPrice = f["price"]?.trimmedString() ?: ""
        val price = fPrice.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val file = MultipartBody.Part.createFormData(
            "file", bitmapFile.name, bitmapFile
                .asRequestBody("image/jpeg".toMediaTypeOrNull())
        )
        return api.createMyArticle(name, description, price, file)
    }

    private fun Bitmap.asFile(cacheDir: File): File {
        val file = File(
            cacheDir,
            "uploadImage"
        )
        file.createNewFile()

        val bos = ByteArrayOutputStream()
        this.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val bitmapData = bos.toByteArray()

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
        } catch (e: FileNotFoundException) {
            Log.d(TAG, "asFile: exception with file happened")
        }
        try {
            fos?.write(bitmapData)
            fos?.flush()
            fos?.close()
        } catch (e: IOException) {
            Log.d(TAG, "asFile: exception with writing to file happened")
        }
        return file
    }
}