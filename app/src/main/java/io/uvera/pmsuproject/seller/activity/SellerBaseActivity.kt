package io.uvera.pmsuproject.seller.activity

import android.app.Activity
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import io.uvera.pmsuproject.AppBaseActivity
import io.uvera.pmsuproject.MainActivity
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.util.intentOf
import io.uvera.pmsuproject.util.selectItemActivity
import io.uvera.pmsuproject.viewmodel.AuthViewModel
import kotlin.reflect.KClass

abstract class SellerBaseActivity : AppBaseActivity() {
    private val authViewModel: AuthViewModel by viewModels()

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_seller_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sellerMenuLogOut -> {
                authViewModel.logOut()
                finish()
            }
            R.id.sellerMenuMyArticles -> {
                selectItemActivity(SellerArticlesActivity::class)
            }
            R.id.sellerMenuAddArticle -> {
                selectItemActivity(
                    SellerUploadArticleActivity::class,
                    clearAll = false,
                    finish = false
                )
            }
            R.id.sellerMenuComments -> {
                selectItemActivity(SellerCommentsActivity::class)
            }
            R.id.sellerMenuSales -> {
                selectItemActivity(SellerSalesActivity::class)
            }
            R.id.sellerMenuAddSale -> {
                selectItemActivity(
                    SellerAddSaleActivity::class,
                    clearAll = false,
                    finish = false
                )
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

}