package io.uvera.pmsuproject.seller.api

import com.skydoves.sandwich.ApiResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import java.time.LocalDate

private const val BASE_PATH = "/seller-sales"

interface SellerSalesApi {
    @GET(BASE_PATH)
    suspend fun getAllSales(): ApiResponse<List<SellerSale>>

    @POST(BASE_PATH)
    suspend fun addSale(@Body body: SellerAddSale): ApiResponse<Void>

}

class SellerAddSale(
    val percent: Int,
    val startDate: String,
    val endDate: String,
    val text: String,
    val articles: List<Long>,
)

class SellerSale(
    val percent: Int,
    val startDate: String,
    val endDate: String,
    val text: String,
    val articleList: List<String>,
)
