package io.uvera.pmsuproject

import android.app.Application
import com.skydoves.sandwich.coroutines.CoroutinesResponseCallAdapterFactory
import io.uvera.pmsuproject.admin.api.AdminPanelApi
import io.uvera.pmsuproject.api.AuthInterceptor
import io.uvera.pmsuproject.api.AuthenticationApi
import io.uvera.pmsuproject.api.AuthenticationService
import io.uvera.pmsuproject.api.RegistrationApi
import io.uvera.pmsuproject.buyer.api.BuyerBrowseApi
import io.uvera.pmsuproject.buyer.api.BuyerMyOrdersApi
import io.uvera.pmsuproject.buyer.api.BuyerPurchaseApi
import io.uvera.pmsuproject.seller.api.SellerCommentsApi
import io.uvera.pmsuproject.seller.api.SellerMyArticlesApi
import io.uvera.pmsuproject.seller.api.SellerSalesApi
import io.uvera.pmsuproject.seller.viewmodel.SellerUploadArticleViewModel
import io.uvera.pmsuproject.util.create
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.format.DateTimeFormatter

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@MyApplication)
            modules(appModule)
        }
    }
}

val globalDateFormatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE

const val BASE_URL = "http://192.168.100.22:8080"

val appModule = module {
    single {
        AuthInterceptor()
    }
    single {
        OkHttpClient.Builder().apply {
            addInterceptor {
                val reqBuilder = it.request().newBuilder().apply {
                    header("Content-Type", "application/json")
                    header("Accept", "application/json")
                }
                return@addInterceptor it.proceed(reqBuilder.build())
            }
            addInterceptor(get<AuthInterceptor>())
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            addInterceptor {
                val req = it.request()
                val response = it.proceed(req)
                if (response.code == 401 && !req.url.toUrl().toString().contains("/auth")) {
                    get<AuthenticationService>().logOut()
                }
                response
            }
        }.build()
    }
    single {
        Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutinesResponseCallAdapterFactory())
            .client(get())
            .build()
    }
    single {
        get<Retrofit>().create(AuthenticationApi::class)
    }
    single {
        get<Retrofit>().create(RegistrationApi::class)
    }
    single {
        AuthenticationService(androidApplication(), get(), get())
    }
    single {
        get<Retrofit>().create(AdminPanelApi::class)
    }
    single {
        get<Retrofit>().create(SellerMyArticlesApi::class)
    }
    single {
        get<Retrofit>().create(SellerCommentsApi::class)
    }
    single {
        get<Retrofit>().create(SellerSalesApi::class)
    }
    single {
        get<Retrofit>().create(BuyerBrowseApi::class)
    }
    single {
        get<Retrofit>().create(BuyerMyOrdersApi::class)
    }
    single {
        get<Retrofit>().create(BuyerPurchaseApi::class)
    }
    viewModel {
        SellerUploadArticleViewModel(androidApplication())
    }
}