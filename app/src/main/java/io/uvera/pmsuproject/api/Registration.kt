package io.uvera.pmsuproject.api

import com.skydoves.sandwich.ApiResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface RegistrationApi {
    @POST("/register/seller")
    suspend fun registerAsSeller(@Body req: RegisterAsSellerRequest) : ApiResponse<Void>

    @POST("/register/buyer")
    suspend fun registerAsBuyer(@Body req: RegisterAsBuyerRequest) : ApiResponse<Void>
}

class RegisterAsBuyerRequest(
    val firstName: String,
    val lastName: String,
    val username: String,
    val address: String,
    val password: String
)

class RegisterAsSellerRequest(
    val firstName: String,
    val lastName: String,
    val username: String,
    val password: String,
    val address: String,
    val email: String,
    val name: String,
)

data class EmptyResponse(val message: String?)