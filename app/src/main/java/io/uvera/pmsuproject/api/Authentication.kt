package io.uvera.pmsuproject.api

import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.auth0.android.jwt.JWT
import com.skydoves.sandwich.ApiResponse
import io.uvera.pmsuproject.MainActivity
import io.uvera.pmsuproject.util.getStringOrNull
import io.uvera.pmsuproject.util.intentOf
import okhttp3.Interceptor
import org.koin.core.component.KoinComponent
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationApi {
    @POST("/auth/authenticate")
    suspend fun authenticate(@Body req: AuthenticationRequest): ApiResponse<AuthenticationResponse>
}

class AuthenticationService(
    private val app: Application,
    private val api: AuthenticationApi,
    private val interceptor: AuthInterceptor,
) : KoinComponent {
    suspend fun authenticate(username: String, password: String) = api.authenticate(
        AuthenticationRequest(username, password)
    )

    fun storeToken(token: String) = app
        .getSharedPreferences("prefs", Context.MODE_PRIVATE).run {
            with(edit()) {
                putString("token", token)
                apply()
            }
        }

    private fun clearToken() = app.getSharedPreferences("prefs", Context.MODE_PRIVATE).run {
        with(edit()) {
            remove("token")
            apply()
        }
    }

    fun logOut() {
        interceptor.clearToken()
        clearToken()
        app.startActivity(app.intentOf(MainActivity::class).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        })
    }

    val token: String?
        get() = app.getSharedPreferences("prefs", Context.MODE_PRIVATE).run {
            getStringOrNull("token")
        }

    val isAuthenticated: Boolean
        get() = app.getSharedPreferences("prefs", Context.MODE_PRIVATE).run {
            getStringOrNull("token")
        } != null

    val role: Role?
        get() = app.getSharedPreferences("prefs", Context.MODE_PRIVATE).run {
            val token = getStringOrNull("token") ?: return null
            val jwt = JWT(token)
            val roles = jwt.getClaim("roles").asList(String::class.java)
            return when {
                roles.contains("SELLER") -> Role.SELLER
                roles.contains("BUYER") -> Role.BUYER
                roles.contains("ADMIN") -> Role.ADMIN
                else -> null
            }
        }
}

class AuthInterceptor : Interceptor {
    private val TAG: String = this.javaClass.simpleName
    private var token: String? = null
    fun setToken(t: String) {
        token = t
    }

    fun clearToken() {
        token = null
    }

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val reqBuilder = chain.request().newBuilder().apply {
            if (token != null) {
                Log.d(TAG, "intercept auth: with token $token")
                header("Authorization", "Bearer $token")
            }
        }
        return chain.proceed(reqBuilder.build())
    }
}

enum class Role {
    BUYER, SELLER, ADMIN
}

data class AuthenticationRequest(val username: String, val password: String)
data class AuthenticationResponse(val token: String)