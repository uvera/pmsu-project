package io.uvera.pmsuproject

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

abstract class AppBaseActivity : AppCompatActivity() {
    private var pressedTime: Long = 0

    override fun onBackPressed() = if (isTaskRoot) {
        if (pressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(baseContext, "Press back again to exit", Toast.LENGTH_SHORT).show();
        }
        pressedTime = System.currentTimeMillis();
    } else {
        super.onBackPressed();
    }
}