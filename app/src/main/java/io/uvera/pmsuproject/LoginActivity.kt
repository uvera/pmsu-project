package io.uvera.pmsuproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            replace(R.id.fragmentLogin, LoginFragment())
        }
    }

}
