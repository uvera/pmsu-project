package io.uvera.pmsuproject.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import android.widget.Button
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import com.afollestad.vvalidator.field.FieldValue
import com.google.android.material.snackbar.Snackbar
import io.uvera.pmsuproject.R
import kotlinx.coroutines.*
import retrofit2.Retrofit
import www.sanju.motiontoast.MotionToast
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

fun SharedPreferences.getStringOrNull(key: String): String? = this.getString(key, null)

suspend inline fun <T> ui(
    crossinline block: CoroutineScope.() -> T
): T = withContext(Dispatchers.Main) { block() }

suspend inline fun <T> io(
    crossinline block: CoroutineScope.() -> T
): T = withContext(Dispatchers.IO) { block() }

inline fun CoroutineScope.ioLaunch(
    context: CoroutineContext = Dispatchers.IO,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    crossinline block: suspend CoroutineScope.() -> Unit
): Job = this.launch(context, start) { block() }

fun Context.intentOf(activity: KClass<*>) = Intent(this, activity.java)

fun <T : Any> Retrofit.create(kClass: KClass<T>): T = this.create(kClass.java)

fun Button.setGrayed(value: Boolean) = this.apply {
    isClickable = !value
    alpha = if (value)
        0.3f
    else
        1f
}

inline fun Fragment.commitTransaction(
    allowStateLoss: Boolean = false,
    crossinline body: FragmentTransaction.() -> Unit
) = this.activity?.supportFragmentManager?.commit(allowStateLoss) {
    body()
}

fun View.snackShort(text: String) =
    Snackbar.make(this, text, Snackbar.LENGTH_SHORT).show()

fun View.snackLong(text: String) =
    Snackbar.make(this, text, Snackbar.LENGTH_LONG).show()


fun <T> FieldValue<T>.trimmedString() where T : Any = this.asString().trim()

fun Context.getMotionToastFont() = ResourcesCompat.getFont(this, R.font.helvetica_regular)

fun parseLocalDateOrNull(text: CharSequence, formatter: DateTimeFormatter) = try {
    LocalDate.parse(text, formatter)
} catch (e: Exception) {
    null
}

fun Activity.selectItemActivity(
    kClass: KClass<*>,
    clearAll: Boolean = true,
    finish: Boolean = true
) {
    if (this::class != kClass) {
        startActivity(this.intentOf(kClass).also { i ->
            if (clearAll)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        })
        if (finish)
            finish()
    }
}


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Activity.motionToastApiError(message: String = "Unknown error occurred") {
    MotionToast.createColorToast(
        this,
        "Error",
        message,
        MotionToast.TOAST_ERROR,
        MotionToast.GRAVITY_BOTTOM,
        MotionToast.SHORT_DURATION,
        this.getMotionToastFont()
    )
}

fun Activity.motionToastApiException(message: String = "Network error") {
    MotionToast.createColorToast(
        this,
        "Error",
        message,
        MotionToast.TOAST_ERROR,
        MotionToast.GRAVITY_BOTTOM,
        MotionToast.SHORT_DURATION,
        this.getMotionToastFont()
    )
}

fun Activity.motionToastSuccess(message: String, title: String = "Success") {
    MotionToast.createColorToast(
        this, "Info",
        message,
        MotionToast.TOAST_SUCCESS,
        MotionToast.GRAVITY_BOTTOM,
        MotionToast.SHORT_DURATION,
        this.getMotionToastFont()
    )
}