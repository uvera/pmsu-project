package io.uvera.pmsuproject.util

import android.hardware.SensorManager
import com.squareup.seismic.ShakeDetector
import io.uvera.pmsuproject.buyer.activity.BuyerBaseActivity
import io.uvera.pmsuproject.seller.activity.SellerBaseActivity

abstract class ShakeableBuyerBaseActivity : BuyerBaseActivity(), ShakeDetector.Listener {
    private var shakeDetector: ShakeDetector? = null
    private var sensorManager: SensorManager? = null

    override fun onResume() {
        val sensorManager: SensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        shakeDetector = ShakeDetector(this)
        shakeDetector?.start(sensorManager)
        super.onResume()
    }

    override fun onPause() {
        shakeDetector?.stop()
        sensorManager?.unregisterListener(shakeDetector)
        super.onPause()
    }
}

abstract class ShakeableSellerBaseActivity : SellerBaseActivity(), ShakeDetector.Listener {
    private var shakeDetector: ShakeDetector? = null
    private var sensorManager: SensorManager? = null

    override fun onResume() {
        val sensorManager: SensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        shakeDetector = ShakeDetector(this)
        shakeDetector?.start(sensorManager)
        super.onResume()
    }

    override fun onPause() {
        shakeDetector?.stop()
        sensorManager?.unregisterListener(shakeDetector)
        super.onPause()
    }
}
