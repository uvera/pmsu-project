package io.uvera.pmsuproject.util

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import androidx.annotation.IdRes
import java.time.LocalDate
import java.util.*

private class DatePickerOnEditText(private val ctx: Context, @IdRes editTextViewId: Int) :
    View.OnClickListener,
    DatePickerDialog.OnDateSetListener {
    private val editText: EditText = (ctx as Activity).findViewById(editTextViewId)
    private var day: Int = 0
    private var month: Int = 0
    private var year: Int = 0

    init {
        editText.setOnClickListener(this)
        editText.keyListener = null
        editText.setText("Input date...")
    }

    override fun onClick(v: View?) {
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        DatePickerDialog(
            ctx,
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
            .show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month + 1
        this.day = dayOfMonth
        updateEditView()
    }

    private fun updateEditView() {
        val text = LocalDate.of(year, month, day).toString()
        editText.setText(text)
    }
}

fun Activity.bindEditTextWithDatePicker(@IdRes id: Int) {
    DatePickerOnEditText(this, id)
}
