package io.uvera.pmsuproject.admin.recyclerview

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.admin.api.AdminPanelApi
import io.uvera.pmsuproject.admin.api.AdminPanelUser
import io.uvera.pmsuproject.util.ioLaunch
import io.uvera.pmsuproject.util.setGrayed
import io.uvera.pmsuproject.util.snackLong
import io.uvera.pmsuproject.util.ui
import kotlinx.coroutines.CoroutineScope

class AdminPanelUsersAdapter(
    private val lifecycleScope: CoroutineScope,
    private val api: AdminPanelApi
) :
    RecyclerView
    .Adapter<AdminPanelUserViewHolder>() {
    private var dataSet: List<AdminPanelUser> = listOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminPanelUserViewHolder =
        AdminPanelUserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_admin_panel_users, parent, false
            ), lifecycleScope, api
        )

    override fun onBindViewHolder(holder: AdminPanelUserViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size

    fun setItems(list: List<AdminPanelUser>) {
        dataSet = list
        notifyDataSetChanged()
    }

}

class AdminPanelUserViewHolder(
    private val v: View,
    private val lifecycleScope: CoroutineScope,
    private val api: AdminPanelApi
) : RecyclerView
.ViewHolder(v) {
    private val firstNameTextView: TextView = v.findViewById(R.id.adminPanelUsersFirstName)
    private val lastNameTextView: TextView = v.findViewById(R.id.adminPanelUsersLastName)
    private val usernameTextView: TextView = v.findViewById(R.id.adminPanelUsersUsername)
    private val button: Button = v.findViewById(R.id.adminPanelUsersButton)

    private var current: AdminPanelUser? = null

    fun bind(it: AdminPanelUser) {
        current = it
        firstNameTextView.text = it.firstName
        lastNameTextView.text = it.lastName
        usernameTextView.text = it.username
        button.text = if (it.active) "Disable user" else "Enable user"
        button.setBackgroundColor(if (it.active) Color.RED else Color.GREEN)
    }

    init {
        button.setOnClickListener {
            current?.let { c ->
                clickButton(c)
            }
        }
    }

    private fun reInitButton() {
        val it = current ?: return
        button.text = if (it.active) "Disable user" else "Enable user"
        button.setBackgroundColor(if (it.active) Color.RED else Color.GREEN)
    }

    private fun clickButton(user: AdminPanelUser) {
        if (user.active) disableAction() else enableAction()
    }

    private fun disableAction() = lifecycleScope.ioLaunch {
        val it = current ?: return@ioLaunch

        ui {
            button.setGrayed(true)
        }

        api.disableUser(it.username)
            .suspendOnSuccess {
                ui {
                    it.active = false
                    reInitButton()
                }
            }
            .suspendOnError {
                ui {
                    v.snackLong("Error occurred")
                }
            }
            .suspendOnException {
                ui {
                    v.snackLong("Network error")
                }
            }

        ui {
            button.setGrayed(false)
        }
    }


    private fun enableAction() = lifecycleScope.ioLaunch {
        val it = current ?: return@ioLaunch

        ui {
            button.setGrayed(true)
        }

        api.enableUser(it.username)
            .suspendOnSuccess {
                ui {
                    it.active = true
                    reInitButton()
                }
            }
            .suspendOnError {
                ui {
                    v.snackLong("Error occurred")
                }
            }
            .suspendOnException {
                ui {
                    v.snackLong("Network error")
                }
            }

        ui {
            button.setGrayed(false)
        }
    }


}