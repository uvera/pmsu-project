package io.uvera.pmsuproject.admin.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.MainActivity
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.admin.api.AdminPanelApi
import io.uvera.pmsuproject.admin.recyclerview.AdminPanelUsersAdapter
import io.uvera.pmsuproject.util.*
import io.uvera.pmsuproject.viewmodel.AuthViewModel
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class AdminActivity : AppCompatActivity() {
    private val api: AdminPanelApi by inject()
    private val authViewModel: AuthViewModel by viewModels()

    private lateinit var adapter: AdminPanelUsersAdapter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_admin_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.adminMenuLogOut -> {
                authViewModel.logOut()
                finish()
                true
            }
            R.id.adminMenuRefresh -> {
                fetchFromApiAndSet()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        val rv: RecyclerView = findViewById(R.id.recyclerViewAdminPanelUsers)
        adapter = AdminPanelUsersAdapter(lifecycleScope, api)
        rv.layoutManager = LinearLayoutManager(this)
        rv.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rv.adapter = adapter

        fetchFromApiAndSet()
    }

    private fun fetchFromApiAndSet() = lifecycleScope.ioLaunch {
        api.getAllUsers()
            .suspendOnSuccess {
                ui {
                    data?.let { adapter.setItems(it) }
                }
            }.suspendOnError {
                ui {
                    this@AdminActivity.motionToastApiError()
                }
            }.suspendOnException {
                ui {
                    this@AdminActivity.motionToastApiException()
                }
            }
    }
}
