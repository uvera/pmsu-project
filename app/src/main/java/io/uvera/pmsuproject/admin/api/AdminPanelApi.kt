package io.uvera.pmsuproject.admin.api

import com.skydoves.sandwich.ApiResponse
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface AdminPanelApi {
    @GET("/user")
    suspend fun getAllUsers(): ApiResponse<List<AdminPanelUser>>

    @DELETE("/user/active/{username}")
    suspend fun disableUser(@Path("username") username: String): ApiResponse<Void>

    @PUT("/user/active/{username}")
    suspend fun enableUser(@Path("username") username: String): ApiResponse<Void>
}

class AdminPanelUser(
    val firstName: String,
    val lastName: String,
    val username: String,
    var active: Boolean,
)