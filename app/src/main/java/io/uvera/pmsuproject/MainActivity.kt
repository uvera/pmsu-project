package io.uvera.pmsuproject

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import io.uvera.pmsuproject.admin.activity.AdminActivity
import io.uvera.pmsuproject.api.Role
import io.uvera.pmsuproject.buyer.activity.BuyerBrowseActivity
import io.uvera.pmsuproject.seller.activity.SellerArticlesActivity
import io.uvera.pmsuproject.util.intentOf
import io.uvera.pmsuproject.util.ui
import io.uvera.pmsuproject.viewmodel.AuthViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val viewModel: AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide();

        lifecycleScope.launch {
            delay(1000L)
            ui {
                with(this@MainActivity)
                {
                    if (viewModel.isAuthenticated) {
                        viewModel.populateInterceptor()

                        when (viewModel.role) {
                            Role.SELLER -> startActivity(this.intentOf(SellerArticlesActivity::class))
                            Role.BUYER -> startActivity(this.intentOf(BuyerBrowseActivity::class))
                            Role.ADMIN -> startActivity(this.intentOf(AdminActivity::class))
                        }
                    } else startActivity(this.intentOf(LoginActivity::class))
                }
                finish()
            }
        }
    }
}