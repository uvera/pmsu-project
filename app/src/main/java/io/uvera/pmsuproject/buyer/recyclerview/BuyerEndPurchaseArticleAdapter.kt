package io.uvera.pmsuproject.buyer.recyclerview

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.uvera.pmsuproject.R

class BuyerEndPurchaseArticleAdapter : RecyclerView.Adapter<BuyerEndPurchaseViewHolder>() {
    private var dataSet = listOf<String>()

    fun setItems(list: List<String>) {
        dataSet = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuyerEndPurchaseViewHolder =
        BuyerEndPurchaseViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_buyer_end_purchase_article, parent, false
            )
        )

    override fun onBindViewHolder(holder: BuyerEndPurchaseViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size

}

class BuyerEndPurchaseViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    private val textView: TextView = v.findViewById(R.id.endPurchaseArticleTextView)
    fun bind(it: String) {
        textView.text = it
    }
}