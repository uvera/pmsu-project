package io.uvera.pmsuproject.buyer.activity

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BrowseSellersArticleModel
import io.uvera.pmsuproject.buyer.api.BuyerBrowseApi
import io.uvera.pmsuproject.buyer.api.EndPurchaseArticle
import io.uvera.pmsuproject.buyer.recyclerview.BuyerPreviewArticlesAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class BuyerBrowseSellersArticlesActivity : BuyerBaseActivity() {
    private val api: BuyerBrowseApi by inject()

    private lateinit var button: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var nothingToShow: TextView
    private var id by Delegates.notNull<Long>()

    private var articles: List<BrowseSellersArticleModel> by Delegates.observable(
        listOf()
    ) { _: KProperty<*>, _: List<BrowseSellersArticleModel>, new: List<BrowseSellersArticleModel> ->
        adapter.setItems(new)
    }


    private val adapter: BuyerPreviewArticlesAdapter = BuyerPreviewArticlesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_browse_sellers_articles)
        supportActionBar?.hide()
        button = findViewById(R.id.buyerPreviewArticlesEndButton)
        button.setOnClickListener {
            clickEndPurchase()
        }
        recyclerView = findViewById(R.id.recyclerViewBuyerSellersArticles)
        nothingToShow = findViewById(R.id.buyerPreviewArticlesNoneTextView)
        id = intent.getLongExtra("id", 0)
        recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = adapter
        }
        nothingToShow.gone()
        button.gone()
        recyclerView.gone()
        fetchFromApiAndSet(id)
    }

    private fun clickEndPurchase() {
        val chosenArticles = articles.filter { it.amount > 0 }
        if (chosenArticles.isEmpty()) {
            this.motionToastApiError("You have to pick at least a single article")
            return
        }
        val intent = this.intentOf(BuyerFinalizePurchaseActivity::class)
            .also { i ->
                i.putParcelableArrayListExtra(
                    "articles", ArrayList(
                        chosenArticles.map {
                            EndPurchaseArticle(
                                it.id,
                                it.name,
                                if (it.salePrice > 0) it.salePrice else it.price,
                                it.amount
                            )
                        })
                )
            }
        startActivity(intent)
    }

    private fun fetchFromApiAndSet(id: Long) = lifecycleScope.ioLaunch {
        api.getSellersArticles(id)
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            button.gone()
                            recyclerView.gone()
                            nothingToShow.visible()
                        } else {
                            button.visible()
                            recyclerView.visible()
                            nothingToShow.gone()
                            articles = d.map { BrowseSellersArticleModel(it) }
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    this@BuyerBrowseSellersArticlesActivity.motionToastApiError()
                }
            }.suspendOnException {
                ui {
                    this@BuyerBrowseSellersArticlesActivity.motionToastApiException()
                }
            }
    }
}