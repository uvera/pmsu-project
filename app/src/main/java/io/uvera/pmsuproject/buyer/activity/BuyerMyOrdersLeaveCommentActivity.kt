package io.uvera.pmsuproject.buyer.activity

import android.content.Intent
import android.hardware.SensorManager
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import com.agilie.agmobilegiftinterface.InterfaceInteractorImpl
import com.agilie.agmobilegiftinterface.gravity.GravityControllerImpl
import com.agilie.agmobilegiftinterface.shake.ShakeBuilder
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import com.squareup.seismic.ShakeDetector
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.viewmodel.BuyerCommentViewModel
import io.uvera.pmsuproject.util.*
import www.sanju.motiontoast.MotionToast
import kotlin.properties.Delegates

class BuyerMyOrdersLeaveCommentActivity : ShakeableBuyerBaseActivity() {
    private lateinit var dropdown: Spinner
    private var id by Delegates.notNull<Long>()

    private val viewModel: BuyerCommentViewModel by viewModels()
    private lateinit var button: Button

    private lateinit var shaker: ShakeBuilder
    private var shaking = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_my_orders_leave_comment)

        button = findViewById(R.id.buyerMyOrdersCommentButton)
        supportActionBar?.hide()
        dropdown = findViewById(R.id.buyerMyOrdersSpinnerRating)
        dropdown.adapter = ArrayAdapter(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            arrayOf(5, 4, 3, 2, 1)
        )
        dropdown.setSelection(0)
        id = intent.getLongExtra("id", 0)
        form {
            spinner(R.id.buyerMyOrdersSpinnerRating, "rating") {
                selection()
            }
            checkable(R.id.buyerMyOrdersCommentAnonCheckBox, "anonymous") {
            }
            input(R.id.buyerMyOrdersCommentComment, "comment") {
                isNotEmpty()
                length().atLeast(1).atMost(500)
            }
            submitWith(R.id.buyerMyOrdersCommentButton) {
                sendFormToApi(it, id)
            }
        }
        shaker = InterfaceInteractorImpl().shake(this).build()
    }

    private fun sendFormToApi(it: FormResult, id: Long) = lifecycleScope.ioLaunch {
        viewModel.sendFormToApi(it, id)
            .suspendOnSuccess {
                ui {
                    this@BuyerMyOrdersLeaveCommentActivity
                        .startActivity(this@BuyerMyOrdersLeaveCommentActivity
                            .intentOf(BuyerMyOrdersActivity::class).also { i ->
                                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            })
                }
            }
            .suspendOnException {
                ui {
                    motionToastException()
                }
            }
            .suspendOnError {
                ui {
                    motionToastError()
                }
            }
    }

    private fun motionToastError() {
        this.motionToastApiError()
    }

    private fun motionToastException() {
        this.motionToastApiException()
    }

    override fun onStop() {
        super.onStop()
        shaker.stopAnimation()
    }

    override fun hearShake() {
        if (shaking) {
            shaker.stopAnimation()
            shaking = false
        } else {
            shaking = true
            shaker.shakeMyActivity()
        }
    }

}
