package io.uvera.pmsuproject.buyer.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.slider.Slider
import io.uvera.pmsuproject.BASE_URL
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BrowseSellersArticleModel

class BuyerPreviewArticlesAdapter : RecyclerView.Adapter<BuyerPreviewArticlesViewHolder>() {
    private var dataSet: List<BrowseSellersArticleModel> = listOf()

    fun setItems(items: List<BrowseSellersArticleModel>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BuyerPreviewArticlesViewHolder =
        BuyerPreviewArticlesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_buyer_sellers_article, parent, false
            )
        )

    override fun onBindViewHolder(holder: BuyerPreviewArticlesViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class BuyerPreviewArticlesViewHolder(
    v: View,
) : RecyclerView.ViewHolder(v) {
    private val nameTextView: TextView = v.findViewById(R.id.buyerPreviewArticleName)
    private val descriptionTextView: TextView = v.findViewById(R.id.buyerPreviewArticleDescription)
    private val priceTextView: TextView = v.findViewById(R.id.buyerPreviewArticlePrice)
    private val articleImageView: ImageView = v.findViewById(R.id.buyerPreviewArticleImage)
    private val slider: Slider = v.findViewById(R.id.buyerPreviewArticleSeekbar)
    private val amount: TextView = v.findViewById(R.id.buyerPreviewArticleAmount)


    private var current: BrowseSellersArticleModel? = null

    fun bind(it: BrowseSellersArticleModel) {
        current = it
        nameTextView.text = it.name
        descriptionTextView.text = it.description
        priceTextView.text =
            if (it.salePrice != 0.0) "on sale ${it.salePrice}\$" else "${it.price}\$"
        Glide.with(itemView.context)
            .load("$BASE_URL/images/${it.pathToImage}")
            .centerCrop()
            .placeholder(R.drawable.loading_spinner)
            .into(articleImageView)
        slider.value = it.amount.toFloat()
    }

    init {
        slider.addOnChangeListener { _: Slider, fl: Float, _: Boolean ->
            current?.let { c ->
                c.amount = fl.toInt()
                amount.text = "Amount: ${c.amount}"
            }
        }
    }

}