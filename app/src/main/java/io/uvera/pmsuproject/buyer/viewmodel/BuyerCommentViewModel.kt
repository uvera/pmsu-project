package io.uvera.pmsuproject.buyer.viewmodel

import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.ApiResponse
import io.uvera.pmsuproject.buyer.api.BuyerMyOrdersApi
import io.uvera.pmsuproject.buyer.api.MyOrder
import io.uvera.pmsuproject.buyer.api.MyOrderComment
import io.uvera.pmsuproject.util.trimmedString
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class BuyerCommentViewModel : ViewModel(), KoinComponent {
    companion object {
        val ratingArray = arrayOf(5, 4, 3, 2, 1)
    }

    private val api: BuyerMyOrdersApi by inject()

    suspend fun sendFormToApi(f: FormResult, id: Long): ApiResponse<Void> {
        val formRating = f["rating"]?.asInt()
        val rating: Int = if (formRating == null) 1 else ratingArray[formRating]
        return api.commentTheOrder(
            id,
            MyOrderComment(
                f["comment"]?.trimmedString() ?: "",
                rating,
                f["anonymous"]?.asBoolean() ?: false
            )
        )
    }
}