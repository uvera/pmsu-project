package io.uvera.pmsuproject.buyer.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BrowseSellersComment

class BuyerBrowseCommentsAdapter : RecyclerView.Adapter<BuyerBrowseCommentsViewHolder>() {
    private var dataSet: List<BrowseSellersComment> = listOf()

    fun setItems(items: List<BrowseSellersComment>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BuyerBrowseCommentsViewHolder =
        BuyerBrowseCommentsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_buyer_browse_comments, parent, false
            )
        )

    override fun onBindViewHolder(holder: BuyerBrowseCommentsViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class BuyerBrowseCommentsViewHolder(
    v: View,
) : RecyclerView.ViewHolder(v) {
    private val senderTextView: TextView = v.findViewById(R.id.buyerCommentsSender)
    private val dateTextView: TextView = v.findViewById(R.id.buyerCommentsDate)
    private val ratingTextView: TextView = v.findViewById(R.id.buyerCommentsRating)
    private val commentTextView: TextView = v.findViewById(R.id.buyerCommentsComment)

    private var current: BrowseSellersComment? = null

    fun bind(it: BrowseSellersComment) {
        current = it
        senderTextView.text = it.sender
        dateTextView.text = it.time
        ratingTextView.text = it.rating.toString()
        commentTextView.text = it.comment
    }
}