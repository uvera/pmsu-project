package io.uvera.pmsuproject.buyer.activity

import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import io.uvera.pmsuproject.AppBaseActivity
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.util.selectItemActivity
import io.uvera.pmsuproject.viewmodel.AuthViewModel

abstract class BuyerBaseActivity : AppBaseActivity() {
    private val authViewModel: AuthViewModel by viewModels()

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_buyer_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.buyerMenuLogOut -> {
                authViewModel.logOut()
                finish()
            }
            R.id.buyerMenuBrowse -> {
                selectItemActivity(BuyerBrowseActivity::class)
            }
            R.id.buyerMenuMyOrders -> {
                selectItemActivity(BuyerMyOrdersActivity::class)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }
}