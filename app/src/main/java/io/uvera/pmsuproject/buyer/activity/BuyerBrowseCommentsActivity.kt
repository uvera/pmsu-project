package io.uvera.pmsuproject.buyer.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BuyerBrowseApi
import io.uvera.pmsuproject.buyer.recyclerview.BuyerBrowseCommentsAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast
import kotlin.properties.Delegates

class BuyerBrowseCommentsActivity : ShakeableBuyerBaseActivity() {
    private lateinit var nothingToBrowse: TextView
    private lateinit var recyclerView: RecyclerView
    private val adapter = BuyerBrowseCommentsAdapter()

    private val api: BuyerBrowseApi by inject()

    private var id by Delegates.notNull<Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_browse_comments)
        supportActionBar?.hide()

        id = intent.getLongExtra("id", 0)

        nothingToBrowse = findViewById(R.id.buyerBrowseCommentsNone)
        recyclerView = findViewById(R.id.recyclerViewBuyerBrowseComments)

        recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = this.adapter
        }

        nothingToBrowse.visibility = View.GONE
        recyclerView.visibility = View.GONE

        fetchFromApiAndSet(id)
    }

    override fun hearShake() = onBackPressed()

    private fun fetchFromApiAndSet(id: Long) = lifecycleScope.ioLaunch {
        api.getSellersComments(id)
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            recyclerView.visibility = View.GONE
                            nothingToBrowse.visibility = View.VISIBLE
                        } else {
                            recyclerView.visibility = View.VISIBLE
                            nothingToBrowse.visibility = View.GONE
                            adapter.setItems(d)
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    recyclerView.visibility = View.GONE
                    nothingToBrowse.visibility = View.VISIBLE
                    this@BuyerBrowseCommentsActivity.motionToastApiError()
                }
            }.suspendOnException {
                ui {
                    recyclerView.visibility = View.GONE
                    nothingToBrowse.visibility = View.VISIBLE
                    this@BuyerBrowseCommentsActivity.motionToastApiException()
                }
            }
    }


}