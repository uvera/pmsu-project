package io.uvera.pmsuproject.buyer.api

import android.os.Parcelable
import com.skydoves.sandwich.ApiResponse
import kotlinx.android.parcel.Parcelize
import retrofit2.http.GET
import retrofit2.http.Path

private const val BASE_PATH = "/browse"

interface BuyerBrowseApi {
    @GET("$BASE_PATH/sellers")
    suspend fun getSellers(): ApiResponse<List<BrowseSeller>>

    @GET("$BASE_PATH/seller/{id}/comments")
    suspend fun getSellersComments(@Path("id") id: Long): ApiResponse<List<BrowseSellersComment>>

    @GET("$BASE_PATH/seller/{id}/articles")
    suspend fun getSellersArticles(@Path("id") id: Long): ApiResponse<List<BrowseSellersArticle>>
}

class BrowseSeller(
    val id: Long,
    val name: String,
    val address: String,
    val email: String,
    val averageScore: Double
)

class BrowseSellersComment(
    val time: String,
    val comment: String,
    val rating: Int,
    val sender: String,
)

open class BrowseSellersArticle(
    val id: Long,
    val name: String,
    val description: String,
    val price: Double,
    val pathToImage: String,
    val salePrice: Double,
)

class BrowseSellersArticleModel(
    id: Long,
    name: String,
    description: String,
    price: Double,
    pathToImage: String,
    salePrice: Double,
    var amount: Int
) : BrowseSellersArticle(id, name, description, price, pathToImage, salePrice) {
    constructor(it: BrowseSellersArticle) :
            this(
                it.id, it.name,
                it.description, it.price,
                it.pathToImage, it.salePrice, 0
            )
}

@kotlinx.parcelize.Parcelize
class EndPurchaseArticle(
    val id: Long,
    val name: String,
    val finalPrice: Double,
    val amount: Int
) : Parcelable