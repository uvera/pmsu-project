package io.uvera.pmsuproject.buyer.api

import com.skydoves.sandwich.ApiResponse
import retrofit2.http.*

private const val BASE_PATH = "/my-orders"

interface BuyerMyOrdersApi {
    @GET(BASE_PATH)
    suspend fun getMyOrders(): ApiResponse<List<MyOrder>>

    @POST("$BASE_PATH/{id}")
    suspend fun confirmOrder(@Path("id") id: Long): ApiResponse<Void>

    @PUT("$BASE_PATH/{id}")
    suspend fun commentTheOrder(@Path("id") id: Long, @Body body: MyOrderComment): ApiResponse<Void>
}

class MyOrderComment(
    val text: String,
    val rating: Int,
    val anonymous: Boolean,
)

class MyOrder(
    val id: Long,
    val time: String,
    val delivered: Boolean,
    val articles: List<String>,
)