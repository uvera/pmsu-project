package io.uvera.pmsuproject.buyer.api

import com.skydoves.sandwich.ApiResponse
import retrofit2.http.Body
import retrofit2.http.POST

private const val BASE_PATH = "/purchase"

interface BuyerPurchaseApi {
    @POST(BASE_PATH)
    suspend fun performPurchase(@Body body: PerformPurchaseDTO): ApiResponse<Void>
}

class PerformPurchaseDTO(
    val list: List<PurchaseArticle>
)

class PurchaseArticle(
    val id: Long,
    val amount: Int,
)
