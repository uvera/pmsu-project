package io.uvera.pmsuproject.buyer.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.activity.BuyerMyOrdersLeaveCommentActivity
import io.uvera.pmsuproject.buyer.api.BuyerMyOrdersApi
import io.uvera.pmsuproject.buyer.api.MyOrder
import io.uvera.pmsuproject.util.intentOf
import io.uvera.pmsuproject.util.ioLaunch
import io.uvera.pmsuproject.util.ui
import www.sanju.motiontoast.MotionToast

class BuyerMyOrdersAdapter(
    private val api: BuyerMyOrdersApi,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val motionToastError: () -> Unit,
    private val motionToastException: () -> Unit,
    private val refreshFunction: () -> Unit
) : RecyclerView.Adapter<BuyerMyOrdersViewHolder>() {
    private var dataSet: List<MyOrder> = listOf()

    fun setItems(items: List<MyOrder>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuyerMyOrdersViewHolder =
        BuyerMyOrdersViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_buyer_my_orders, parent, false
            ), api, lifecycleScope, refreshFunction, motionToastError, motionToastException
        )

    override fun onBindViewHolder(holder: BuyerMyOrdersViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class BuyerMyOrdersViewHolder(
    v: View,
    private val api: BuyerMyOrdersApi,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val refreshFunction: () -> Unit,
    private val motionToastError: () -> Unit,
    private val motionToastException: () -> Unit,
) : RecyclerView.ViewHolder(v) {
    private val orderDate: TextView = v.findViewById(R.id.buyerMyOrdersOrderDate)
    private val articles: TextView = v.findViewById(R.id.buyerMyOrdersArticles)
    private val button: Button = v.findViewById(R.id.buyerMyOrdersActionButton)

    private var current: MyOrder? = null

    fun bind(it: MyOrder) {
        current = it
        orderDate.text = it.time
        articles.text = it.articles.joinToString(", ")
        button.text = if (!it.delivered) "Confirm delivery" else "Leave comment"
    }

    init {
        button.setOnClickListener {
            current?.let { c ->
                if (c.delivered) clickForComment(c.id) else clickForConfirm(c.id)
            }
        }
    }

    private fun clickForConfirm(id: Long) = lifecycleScope.ioLaunch {
        api.confirmOrder(id)
            .suspendOnSuccess {
                refreshFunction()
            }
            .suspendOnError {
                ui {
                    motionToastError()
                }
            }.suspendOnException {
                ui {
                    motionToastException()
                }
            }
    }

    private fun clickForComment(id: Long) = with(itemView.context) {
        val intent = intentOf(BuyerMyOrdersLeaveCommentActivity::class).also { i ->
            i.putExtra("id", id)
        }
        startActivity(intent)
    }

}