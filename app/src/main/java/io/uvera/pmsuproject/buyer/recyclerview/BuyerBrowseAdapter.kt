package io.uvera.pmsuproject.buyer.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.activity.BuyerBrowseCommentsActivity
import io.uvera.pmsuproject.buyer.activity.BuyerBrowseSellersArticlesActivity
import io.uvera.pmsuproject.buyer.api.BrowseSeller
import io.uvera.pmsuproject.util.intentOf
import kotlinx.coroutines.withContext
import java.math.RoundingMode

class BuyerBrowseAdapter : RecyclerView.Adapter<BuyerBrowseViewHolder>() {
    private var dataSet: List<BrowseSeller> = listOf()

    fun setItems(items: List<BrowseSeller>) {
        dataSet = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuyerBrowseViewHolder =
        BuyerBrowseViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_buyer_browse, parent, false
            )
        )

    override fun onBindViewHolder(holder: BuyerBrowseViewHolder, position: Int) =
        dataSet[position].let {
            holder.bind(it)
        }

    override fun getItemCount(): Int = dataSet.size
}

class BuyerBrowseViewHolder(
    v: View,
) : RecyclerView.ViewHolder(v) {
    private val nameTextView: TextView = v.findViewById(R.id.buyerBrowseName)
    private val addressTextView: TextView = v.findViewById(R.id.buyerBrowseAddress)
    private val ratingTextView: TextView = v.findViewById(R.id.buyerBrowseRating)
    private val showArticlesButton: Button = v.findViewById(R.id.buyerBrowseShowArticles)
    private val showSellerInfoButton: Button = v.findViewById(R.id.buyerBrowseShowSellerInfo)

    private var current: BrowseSeller? = null

    fun bind(it: BrowseSeller) {
        current = it
        nameTextView.text = it.name
        addressTextView.text = it.address
        ratingTextView.text =
            it.averageScore.toBigDecimal().setScale(1, RoundingMode.HALF_EVEN).toString()

    }

    init {
        showArticlesButton.setOnClickListener {
            current?.let { c ->
                clickArticles(c)
            }
        }
        showSellerInfoButton.setOnClickListener {
            current?.let { c ->
                clickInfo(c)
            }
        }
    }

    private fun clickArticles(c: BrowseSeller) = with(itemView.context) {
        val intent = intentOf(BuyerBrowseSellersArticlesActivity::class)
            .also { i ->
                i.putExtra("id", c.id)
            }
        startActivity(intent)
    }

    private fun clickInfo(c: BrowseSeller) {
        val ctx = itemView.context
        val intent = ctx.intentOf(BuyerBrowseCommentsActivity::class).also { i ->
            i.putExtra("id", c.id)
        }
        ctx.startActivity(intent)
    }
}