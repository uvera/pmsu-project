package io.uvera.pmsuproject.buyer.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.onSuccess
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BuyerPurchaseApi
import io.uvera.pmsuproject.buyer.api.EndPurchaseArticle
import io.uvera.pmsuproject.buyer.api.PerformPurchaseDTO
import io.uvera.pmsuproject.buyer.api.PurchaseArticle
import io.uvera.pmsuproject.buyer.recyclerview.BuyerEndPurchaseArticleAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import kotlin.properties.Delegates

class BuyerFinalizePurchaseActivity : ShakeableBuyerBaseActivity() {
    private val api: BuyerPurchaseApi by inject()


    private lateinit var articlesToSend: List<EndPurchaseArticle>
    private val adapter: BuyerEndPurchaseArticleAdapter = BuyerEndPurchaseArticleAdapter()
    private lateinit var recyclerView: RecyclerView
    private lateinit var totalTV: TextView
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_finalize_purchase)
        button = findViewById(R.id.performEndPurchase)
        articlesToSend =
            intent.getParcelableArrayListExtra<EndPurchaseArticle>("articles")?.toList() ?: listOf()
        recyclerView = findViewById(R.id.recyclerViewEndPurchase)
        recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = adapter
        }
        totalTV = findViewById(R.id.endPurchaseTotalTextView)
        adapter.setItems(articlesToSend.map { "${it.amount}x ${it.name} (${it.finalPrice}\$ each)" })
        val total: Double =
            articlesToSend.fold(0.0) { d: Double, endPurchaseArticle: EndPurchaseArticle ->
                d + endPurchaseArticle.finalPrice * endPurchaseArticle.amount
            }
        totalTV.text = "Total: $total \$"
        button.setOnClickListener {
            endPurchase()
        }
    }

    override fun hearShake() {
        button.performClick()
    }

    private fun endPurchase() = lifecycleScope.ioLaunch {
        api.performPurchase(PerformPurchaseDTO(
            articlesToSend.map { PurchaseArticle(it.id, it.amount) }
        )).suspendOnSuccess {
            ui {
                with(this@BuyerFinalizePurchaseActivity) {
                    motionToastSuccess("Successfully purchased the items")
                    val intent = this.intentOf(BuyerMyOrdersActivity::class).also { i ->
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    }
                    this.startActivity(intent)
                }
            }
        }.suspendOnException {
            ui {
                this@BuyerFinalizePurchaseActivity.motionToastApiException()
            }
        }.suspendOnError {
            ui {
                this@BuyerFinalizePurchaseActivity.motionToastApiError()
            }
        }
    }
}