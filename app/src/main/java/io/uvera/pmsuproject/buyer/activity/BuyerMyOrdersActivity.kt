package io.uvera.pmsuproject.buyer.activity

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BuyerMyOrdersApi
import io.uvera.pmsuproject.buyer.api.MyOrder
import io.uvera.pmsuproject.buyer.recyclerview.BuyerMyOrdersAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class BuyerMyOrdersActivity : BuyerBaseActivity() {
    private val api: BuyerMyOrdersApi by inject()
    private val adapter: BuyerMyOrdersAdapter = BuyerMyOrdersAdapter(
        api,
        lifecycleScope,
        { motionToastError() },
        { motionToastException() },
        { fetchFromApiAndSet() })
    private lateinit var nothingToShow: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_my_orders)

        nothingToShow = findViewById(R.id.buyerMyOrdersNone)
        recyclerView = findViewById(R.id.recyclerViewBuyerMyOrders)

        recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = adapter
        }

        nothingToShow.gone()
        recyclerView.gone()

        fetchFromApiAndSet()
    }

    private fun motionToastError() {
        this.motionToastApiError()
    }

    private fun motionToastException() {
        this.motionToastApiException()
    }

    private fun fetchFromApiAndSet() = lifecycleScope.ioLaunch {
        api.getMyOrders()
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            recyclerView.gone()
                            nothingToShow.visible()
                        } else {
                            recyclerView.visible()
                            nothingToShow.gone()
                            adapter.setItems(d)
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    recyclerView.gone()
                    nothingToShow.visible()
                    motionToastError()
                }
            }.suspendOnException {
                ui {
                    recyclerView.gone()
                    nothingToShow.visible()
                    motionToastException()
                }
            }
    }

}