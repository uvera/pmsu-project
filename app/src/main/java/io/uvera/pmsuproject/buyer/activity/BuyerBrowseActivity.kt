package io.uvera.pmsuproject.buyer.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.R
import io.uvera.pmsuproject.buyer.api.BuyerBrowseApi
import io.uvera.pmsuproject.buyer.recyclerview.BuyerBrowseAdapter
import io.uvera.pmsuproject.util.*
import org.koin.android.ext.android.inject
import www.sanju.motiontoast.MotionToast

class BuyerBrowseActivity : BuyerBaseActivity() {
    private val api: BuyerBrowseApi by inject()

    private val adapter = BuyerBrowseAdapter()

    private lateinit var nothingToBrowse: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buyer_browse_articles)

        nothingToBrowse = findViewById(R.id.buyerBrowseNone)
        recyclerView = findViewById(R.id.recyclerViewBuyerBrowse)

        recyclerView.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = this.adapter
        }

        nothingToBrowse.visibility = View.GONE
        recyclerView.visibility = View.GONE

        fetchFromApiAndSet()
    }

    override fun onResume() {
        super.onResume()
        fetchFromApiAndSet()
    }

    private fun fetchFromApiAndSet() = lifecycleScope.ioLaunch {
        api.getSellers()
            .suspendOnSuccess {
                ui {
                    data?.let { d ->
                        if (d.isEmpty()) {
                            nothingToBrowse.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        } else {
                            recyclerView.visibility = View.VISIBLE
                            nothingToBrowse.visibility = View.GONE
                            adapter.setItems(d)
                        }
                    }
                }
            }.suspendOnError {
                ui {
                    recyclerView.visibility = View.GONE
                    nothingToBrowse.visibility = View.VISIBLE
                    this@BuyerBrowseActivity.motionToastApiError()
                }
            }.suspendOnException {
                ui {
                    recyclerView.visibility = View.GONE
                    nothingToBrowse.visibility = View.VISIBLE
                    this@BuyerBrowseActivity.motionToastApiException()
                }
            }
    }
}