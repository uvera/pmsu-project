package io.uvera.pmsuproject.viewmodel

import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import io.uvera.pmsuproject.api.RegisterAsBuyerRequest
import io.uvera.pmsuproject.api.RegisterAsSellerRequest
import io.uvera.pmsuproject.api.RegistrationApi
import io.uvera.pmsuproject.util.trimmedString
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class RegisterAsSellerViewModel() : ViewModel(), KoinComponent {
    private val api: RegistrationApi by inject()

    suspend fun register(f: FormResult) = api.registerAsSeller(
        RegisterAsSellerRequest(
            firstName = f["firstName"]?.trimmedString() ?: "",
            lastName = f["lastName"]?.trimmedString() ?: "",
            username = f["username"]?.trimmedString() ?: "",
            password = f["password"]?.trimmedString() ?: "",
            address = f["address"]?.trimmedString() ?: "",
            email = f["email"]?.trimmedString() ?: "",
            name = f["name"]?.trimmedString() ?: "",
        )
    )
}

class RegisterAsBuyerViewModel() : ViewModel(), KoinComponent {
    private val api: RegistrationApi by inject()

    suspend fun register(f: FormResult) = api.registerAsBuyer(
        RegisterAsBuyerRequest(
            firstName = f["firstName"]?.trimmedString() ?: "",
            lastName = f["lastName"]?.trimmedString() ?: "",
            username = f["username"]?.trimmedString() ?: "",
            password = f["password"]?.trimmedString() ?: "",
            address = f["address"]?.trimmedString() ?: "",
        )
    )
}

