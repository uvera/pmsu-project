package io.uvera.pmsuproject.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.afollestad.vvalidator.form.FormResult
import io.uvera.pmsuproject.api.AuthInterceptor
import io.uvera.pmsuproject.api.AuthenticationService
import io.uvera.pmsuproject.api.Role
import io.uvera.pmsuproject.util.trimmedString
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class AuthViewModel() : ViewModel(), KoinComponent {
    private val TAG: String = "AuthViewModel"
    private val service: AuthenticationService by inject()
    private val authInterceptor: AuthInterceptor by inject()
    suspend fun authenticate(f: FormResult) =
        service.authenticate(
            f["username"]?.trimmedString() ?: "",
            f["password"]?.trimmedString() ?: ""
        )

    fun storeToken(token: String) {
        service.storeToken(token)
        authInterceptor.setToken(token)
    }

    fun populateInterceptor() = authInterceptor.setToken(service.token ?: "").run {
        Log.d(TAG, "populateInterceptor: ${service.token}")
    }

    fun logOut() = service.logOut()

    val isAuthenticated: Boolean
        get() = service.isAuthenticated

    val role: Role?
        get() = service.role
}

