package io.uvera.pmsuproject

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import io.uvera.pmsuproject.admin.activity.AdminActivity
import io.uvera.pmsuproject.api.Role
import io.uvera.pmsuproject.buyer.activity.BuyerBrowseActivity
import io.uvera.pmsuproject.seller.activity.SellerArticlesActivity
import io.uvera.pmsuproject.util.*
import io.uvera.pmsuproject.viewmodel.AuthViewModel
import io.uvera.pmsuproject.viewmodel.RegisterAsBuyerViewModel
import io.uvera.pmsuproject.viewmodel.RegisterAsSellerViewModel
import www.sanju.motiontoast.MotionToast

class LoginFragment : Fragment(R.layout.fragment_login_or_register) {

    // viewModel
    private val viewModel: AuthViewModel by viewModels()

    // bindings
    private lateinit var loginButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loginButton = view.findViewById(R.id.loginButton)

        form {
            input(R.id.editTextUsername, name = "username") {
                isNotEmpty()
            }
            input(R.id.editTextPassword, name = "password") {
                isNotEmpty()
            }
            submitWith(R.id.loginButton) {
                submitToApi(it)
            }
        }

        view.findViewById<Button>(R.id.registerAsBuyer).setOnClickListener {
            activity?.supportFragmentManager?.commit {
                replace(R.id.fragmentLogin, RegisterAsBuyerFragment())
                addToBackStack(null)
            }
        }
        view.findViewById<Button>(R.id.registerAsSeller).setOnClickListener {
            activity?.supportFragmentManager?.commit {
                replace(R.id.fragmentLogin, RegisterAsSellerFragment())
                addToBackStack(null)
            }
        }
    }

    private fun submitToApi(f: FormResult) = lifecycleScope.ioLaunch {
        ui {
            loginButton.setGrayed(true)
        }
        viewModel.authenticate(f).suspendOnSuccess {
            ui {
                loginButton.setGrayed(false)
                viewModel.storeToken(data!!.token)
                when (viewModel.role) {
                    Role.SELLER -> startActivity(
                        requireActivity().intentOf(SellerArticlesActivity::class)
                    )
                    Role.BUYER -> startActivity(
                        requireActivity().intentOf(BuyerBrowseActivity::class)
                    )
                    Role.ADMIN -> startActivity(
                        requireActivity().intentOf(AdminActivity::class)
                    )
                }
                activity?.finish()
            }
        }.suspendOnError {
            ui {
                this@LoginFragment.activity?.motionToastApiError("Bad auth")
                loginButton.setGrayed(false)
            }
        }.suspendOnException {
            ui {
                this@LoginFragment.activity?.motionToastApiException()
                loginButton.setGrayed(false)
            }
        }
    }
}

class RegisterAsSellerFragment : Fragment(R.layout.fragment_register_as_seller) {
    // view
    private lateinit var registerButton: Button

    // viewModel
    private val viewModel: RegisterAsSellerViewModel by viewModels()

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        registerButton = v.findViewById(R.id.registerAsSellerButton)
        form {
            useRealTimeValidation()
            input(R.id.registerAsSellerEditTextFirstName, "firstName") {
                isNotEmpty()
            }
            input(R.id.registerAsSellerEditTextLastName, "lastName") {
                isNotEmpty()
            }
            input(R.id.registerAsSellerEditTextUsername, "username") {
                isNotEmpty()
                matches("^[a-z0-9_-]{3,15}\$")
                    .description("Username should be 3-15 characters long.")
            }
            input(R.id.registerAsSellerEditTextPassword, "password") {
                isNotEmpty()
            }
            input(R.id.registerAsSellerEditTextAddress, "address") {
                isNotEmpty()
            }
            input(R.id.registerAsSellerEditTextEmail, "email") {
                isEmail()
            }
            input(R.id.registerAsSellerEditTextBusiness, "name") {
                isNotEmpty()
            }
            submitWith(R.id.registerAsSellerButton) {
                sendToApi(it)
            }
        }
    }

    private fun sendToApi(form: FormResult) = lifecycleScope.ioLaunch {
        ui {
            registerButton.setGrayed(true)
        }
        viewModel.register(form).suspendOnSuccess {
            ui {
                registerButton.setGrayed(false)
                requireActivity().motionToastSuccess("Registration successful")
                commitTransaction {
                    setReorderingAllowed(true)
                    replace(R.id.fragmentLogin, LoginFragment())
                }
            }
        }.suspendOnError {
            ui {
                requireActivity().motionToastApiError("Account with this username already exists.")
                registerButton.setGrayed(false)
            }
        }.suspendOnException {
            ui {
                requireActivity().motionToastApiException()
                registerButton.setGrayed(false)
            }
        }
    }
}

class RegisterAsBuyerFragment : Fragment(R.layout.fragment_register_as_buyer) {
    // view
    private lateinit var registerButton: Button

    // viewModel
    private val viewModel: RegisterAsBuyerViewModel by viewModels()

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        registerButton = v.findViewById(R.id.registerAsBuyerButton)
        form {
            useRealTimeValidation()
            input(R.id.registerAsBuyerFirstName, "firstName") {
                isNotEmpty()
            }
            input(R.id.registerAsBuyerLastName, "lastName") {
                isNotEmpty()
            }
            input(R.id.registerAsBuyerUsername, "username") {
                isNotEmpty()
                matches("^[a-z0-9_-]{3,15}\$")
                    .description("Username should be 3-15 characters long.")
            }
            input(R.id.registerAsBuyerAddress, "address") {
                isNotEmpty()
            }
            input(R.id.registerAsBuyerPassword, "password") {
                isNotEmpty()
            }
            submitWith(R.id.registerAsBuyerButton) {
                sendToApi(it)
            }
        }
    }

    private fun sendToApi(form: FormResult) = lifecycleScope.ioLaunch {
        ui {
            registerButton.setGrayed(true)
        }
        viewModel.register(form).suspendOnSuccess {
            ui {
                registerButton.setGrayed(false)
                requireActivity().motionToastSuccess("Registration successful")
                commitTransaction {
                    setReorderingAllowed(true)
                    replace(R.id.fragmentLogin, LoginFragment())
                }
            }
        }.suspendOnError {
            ui {
                requireActivity().motionToastApiError("Account with this username already exists.")
                registerButton.setGrayed(false)
            }
        }.suspendOnException {
            ui {
                requireActivity().motionToastApiException()
            }
        }
    }
}